package com.company;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

public class RequestHandler implements Callable<List<Integer>> {
    private List<Integer> records;

    public RequestHandler(List<Integer> records) {
        this.records = records;
    }

//    @Override
//    public void run() {
//        // sort the records this thread has access to
//      RadixSort radixSort=new RadixSort(records.stream().mapToInt(Integer::intValue).toArray(),records.size());
//    }

    @Override
    public List<Integer> call() throws Exception {
        RadixSort radixSort=new RadixSort(records.stream().mapToInt(Integer::intValue).toArray(),records.size());
        int[] arr = radixSort.sort();
        return Arrays.stream(arr).boxed().collect(Collectors.toList());
    }
}
